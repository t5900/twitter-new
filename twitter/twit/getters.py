from django.db.models import query
from .models import LikedComments, Tweets, Users, Followers, Retweets, LikedTweets, Comments
from .serializers import TweetsSerializer, RetweetSerializer, LikedTweetsSerializer, FollowersSerializer, UsersSerializer, CommentsSerializer
import itertools

def get_followers(user_id):
    return FollowersSerializer(
        Followers.objects.filter(target_id=user_id).distinct(), 
        many=True).data

def get_followers_ids(user_id):
    return [di['user'] for di in list(Followers.objects.filter(target_id=user_id).values('user'))]

def get_subscriptions(user_id):
    return FollowersSerializer(
        Followers.objects.filter(user=user_id).distinct(),
        many=True).data

def get_subscriptions_ids(user_id):
    return [di['target_id'] for di in list(Followers.objects.filter(user=user_id).values('target_id'))]
    
def get_user_tweetletter(user_id, select_from, select_to):
    ids = [
        di['target_id'] for di in list(
            Followers.objects.filter(user=user_id).values('target_id')
        )
    ]
    tweets_orig_query = Tweets.objects.filter(user__in=ids).order_by('-published','-likes_number').distinct()[select_from:select_to]
    tweets_to_exclude = [di['id'] for di in list(tweets_orig_query.values('id'))]
    return {
        'original': TweetsSerializer(
            tweets_orig_query, 
            many=True).data,
        'retweeted': RetweetSerializer(
            Retweets.objects
            .select_related('tweet_id')
            .filter(user_id__in=ids)
            .exclude(tweet_id__in=tweets_to_exclude)
            .order_by('tweet_id__-published')
            .distinct()[select_from:select_to],
            many=True).data
    }

def get_user_tweetletter_ids(user_id, select_from, select_to):
    ids = [
        di['target_id'] for di in list(
            Followers.objects.filter(user=user_id).values('target_id')
        )
    ][select_from:select_to]
    tweets_orig_query = Tweets.objects.filter(user__in=ids).order_by('-published','-likes_number').values('id').distinct()
    tweets_to_exclude = [di['id'] for di in list(tweets_orig_query)]
    tweets_retweeted = [
            di['tweet_id__id'] for di in list(
                Retweets.objects
                .select_related('tweet_id')
                .filter(user_id__in=ids)
                .exclude(tweet_id__in=tweets_to_exclude)
                .order_by('tweet_id__-published')
                .distinct()
                .values('tweet_id__id')[select_from:select_to]
            )
        ]
    tweets_to_exclude.extend(tweets_retweeted)
    return tweets_to_exclude

def get_user_orig_tweets_count(user_id):
    ids = [
        di['target_id'] for di in list(
            Followers.objects.filter(user=user_id).values('target_id')
        )
    ]
    return Tweets.objects.filter(user_id__in=ids).values('id').count()

def get_user_tweets(user_id):
    return TweetsSerializer(
        Tweets.objects.filter(user=user_id).order_by('-published').distinct(), 
        many=True).data

def get_user_tweets_ids(user_id):
    return [
        di['id'] for di in list(
            Tweets.objects.filter(user=user_id).order_by('-published').distinct().values('id')
        )
    ]

def get_users_who_liked_tweet(tweet_id):
    return LikedTweetsSerializer(
        LikedTweets.objects.filter(tweet_id=tweet_id).distinct(),
        many=True).data

def get_users_who_liked_tweet_ids(tweet_id):
    return [
        di['user_id'] for di in list(
            LikedTweets.objects.filter(tweet_id=tweet_id).distinct().values('user_id')
        )
    ]

def get_comments_for_tweet(tweet_id):
    return [di['id'] for di in list(
        Comments.objects.filter(tweet_id=tweet_id).order_by('-published').values('id')
    )]
        
def get_all_comments():
    query = Comments.objects.all()
    return {
        'comments_number': query.count(),
        'comments': CommentsSerializer(query, many=True).data
    }

def get_user_comments(user_id):
    query = Comments.objects.filter(user=Users.objects.get(id=user_id)).values('id')
    if query:
        return [di['id'] for di in list(query)]
    else:
        return []

def get_all_tweets():
    return TweetsSerializer(
        Tweets.objects.order_by('-published').all(), 
        many=True).data

def get_all_users():
    query = Users.objects.all().order_by('id')
    return {
        'users_number': query.count(),
        'users': UsersSerializer(query, many=True).data
    }

def select_all_tweets_by_tag(tag):
    return [
        di['id'] for di in list(
            Tweets.objects.filter(tags=tag).values('id').order_by('-published').distinct()
        )
    ]

def get_users_who_liked_comment(comment_id):
    return [
        di['user_id'] for di in list(
            LikedComments.objects.filter(comment_id=comment_id).values('user_id')
        )
    ]

def get_all_tweets_count():
    return Tweets.objects.all().values('id').count()

def get_all_comments_count():
    return Comments.objects.all().values('id').count()
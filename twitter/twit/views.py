from django.db.models import query
from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView
from celery.result import AsyncResult
from .tasks import create_task_master
#from .getters import get_users_who_liked_tweet
from .cache import *

class CreateContent(APIView):
    def post(self, request, users_number, format=None):
        task = create_task_master.delay(users_number)
        return Response({"task_id": task.id})

class JobStatus(APIView):
    def post(self, request, format=None):
        task_result = AsyncResult(request.data.get('task_id', ''))
        result = {
            "task_id": request.data.get('task_id', ''),
            "task_status": task_result.status,
            "task_result": task_result.result
        }
        return Response(result)

class ShowFollowers(APIView):
    def get(self, request, user_id, format=None):
        return Response(get_from_cache(f'followers_list_for_user_{user_id}', 'user_'))

class ShowSubscriptions(APIView):
    def get(self, request, user_id, format=None):
        return Response(get_from_cache(f"subscriptions_list_for_user_{user_id}", 'user_'))

class TweetLetter(APIView):
    def get(self, request, user_id, chunk, format=None):
        return Response(
           get_tweets_letter(user_id,chunk)
        )

class UserTweets(APIView):
    def get(self, request, user_id, format=None):
        return Response(get_from_cache(f"user_{user_id}_tweets_list", "tweet_"))

class GetWhoLikedTweet(APIView):
    def get(self, request, tweet_id, format=None):
        return Response(get_from_cache(f"users_who_liked_tweet_{tweet_id}", 'user_'))

class GetTweetsByTag(APIView):
    def get(self, request, tag, format=None):
        return Response(get_from_cache(f"#{tag}_tweets_list", "tweet_"))

class GetUserComments(APIView):
    def get(self, request, user_id, format=None):
        return Response(get_from_cache(f'user_{user_id}_comments', 'comment_'))

class GetCommentsForTweet(APIView):
    def get(self, request, tweet_id, format=None):
        return Response(get_from_cache(f'tweet_{tweet_id}_comments_ids', 'comment_'))

class GetUsersWhoLikedComment(APIView):
    def get(self, request, comment_id, format=None):
        return Response(get_from_cache(f'users_who_liked_comment_{comment_id}', 'user_'))
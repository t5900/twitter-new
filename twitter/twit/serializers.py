from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import Tweets, Users, Retweets, LikedTweets, Followers, Comments, LikedComments

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = (
            "id",
            "name",
            "user_id",
            "followers"
        )

class TweetsSerializer(serializers.ModelSerializer):
    #user = UsersSerializer(read_only=True)
    class Meta:
        model = Tweets
        fields = (
            "id",
            "user",
            "text",
            "likes_number",
            "published",
            "views",
            "retweets_number",
            "comments_number",
            "tags"
        )

class RetweetSerializer(serializers.ModelSerializer):
    #tweet_id = TweetsSerializer(read_only=True)
    class Meta:
        model = Retweets
        fields = [
            "user_id",
            "tweet_id",
            "retweet_date"
        ]

class LikedTweetsSerializer(serializers.ModelSerializer):
    # user_id = UsersSerializer(read_only=True)
    # tweet_id = TweetsSerializer(read_only=True)
    class Meta:
        model = LikedTweets
        fields = (
            "user_id",
            "tweet_id"
        )

class FollowersSerializer(serializers.ModelSerializer):
    # user = UsersSerializer(read_only=True)
    # target_id = UsersSerializer(read_only=True)

    class Meta:
        model = Followers
        fields = (
            "user",
            "target_id"
        )

class CommentsSerializer(serializers.ModelSerializer):
    # user = UsersSerializer(read_only=True)
    # tweet_id = TweetsSerializer(read_only=True)

    class Meta:
        model = Comments
        fields = (
            "id",
            "user",
            "text",
            "likes_number",
            "published",
            "tweet_id"
        )

class LikedCommentsSerializer(serializers.ModelSerializer):
    # user_id = UsersSerializer(read_only=True)
    # comment_id = CommentsSerializer(read_only=True)

    class Meta:
        model = LikedComments
        fields = (
            "user_id",
            "comment_id"
        )
from django.core.cache import cache
from django.conf import settings
from .models import *
from celery import shared_task
from .getters import *
import math

cache_timeout = 86400

#set each user cache
#users = get_all_users()

def add_to_cache(key, value):
    if key not in cache:
        cache.set(key, value, timeout=cache_timeout)

@shared_task
def cache_users(users):
    cache.clear()
    users_ser = users.get('users')
    for user in users_ser:
        add_to_cache(f'user_{user["id"]}', user)
    return "Users cached!"

@shared_task
def cache_tweets(users):
    tweets = get_all_tweets()
    for i in tweets:
        add_to_cache(f"tweet_{i['id']}", i)
    return "Tweets cached!"

@shared_task
def cache_comments(comments):
    for comment in comments:
        add_to_cache(f'comment_{comment["id"]}', comment)

        #set list of users who liked comment
        users_list = get_users_who_liked_comment(int(comment["id"]))
        add_to_cache(f'users_who_liked_comment_{comment["id"]}', users_list)
    return "Comments cached!"

@shared_task
def cache_tags(users):
    #set tweets ids list by tag
    tags = ['#sport', '#music', "#travels", "#photo", '#it', "#russia"]
    for tag in tags:
        tweets_list = select_all_tweets_by_tag(tag)
        add_to_cache(f"{tag}_tweets_list", tweets_list)
    return "Tags cached!"

@shared_task
def cache_user_comments(users):
    users_number = users.get('users_number')
    for user in range(1,users_number):
        #set each user comments
        user_comments = get_user_comments(user)
        add_to_cache(f'user_{user}_comments', user_comments)
    return "Users comments cached!"

@shared_task
def cache_followers_lists(users):
    users_number = users.get('users_number')
    for user in range(1,users_number):
        #set followers list for user
        followers = get_followers_ids(user)
        add_to_cache(f"followers_list_for_user_{user}", followers)
    return "Followers lists cached!"

@shared_task
def cache_subs_lists(users):
    users_number = users.get('users_number')
    for user in range(1,users_number):
        #set user subscriptions list
        subscriptions = get_subscriptions_ids(user)
        add_to_cache(f"subscriptions_list_for_user_{user}", subscriptions)
    return "Subs lists cached!"

@shared_task
def cache_tweets_letters(user):
    #set tweets letter for each user with chunks
    chunks_number = math.ceil(get_user_orig_tweets_count(user)/100)
    select_from = 0
    select_to = 100
    tweets_ids_chunk = []
    for chunk in range(1,chunks_number):
        tweets_ids_chunk = get_user_tweetletter_ids(user,select_from,select_to)
        add_to_cache(f"tweets_ids_for_user_{user}_chunk_{chunk}", tweets_ids_chunk)
        select_from = select_to
        select_to += 100
    return "Tweets letters cached!"

@shared_task
def cache_user_tweets_lists(users):
    users_number = users.get('users_number')
    for user in range(1,users_number):
        #set user tweets list
        user_tweets = get_user_tweets_ids(user)
        add_to_cache(f"user_{user}_tweets_list", user_tweets)
    return "Users tweets ids lists cached!"

@shared_task
def cache_users_who_liked_tweet_lists(users):
    users_number = users.get('users_number')
    for user in range(1,users_number):
        user_tweets = get_user_tweets_ids(user)
        if user_tweets:
            for tweet in user_tweets:
                users_who_liked_tweet = get_users_who_liked_tweet_ids(tweet)
                add_to_cache(f"users_who_liked_tweet_{tweet}", users_who_liked_tweet)
    return "Users who liked tweets ids lists cached!"

@shared_task
def cache_comments_for_tweet(users):
    users_number = users.get('users_number')
    for user in range(1,users_number):
        user_tweets = get_user_tweets_ids(user)
        if user_tweets:
            for tweet in user_tweets:
                #set each tweet comments to cache
                comments_list = get_comments_for_tweet(tweet)
                add_to_cache(f'tweet_{tweet}_comments_ids', comments_list)
    return "Comments for tweets cached!"

def get_tweets_letter(user_id, chunk):
    tweets_cached_chunk_name = f'tweets_for_user_{user_id}_chunk_{chunk}'

    if tweets_cached_chunk_name in cache:
        tweets = cache.get(tweets_cached_chunk_name)
    else:
        ids = f"tweets_ids_for_user_{user_id}_chunk_{chunk}"
        if ids in cache:
            tweets = [cache.get(f"tweet_{id}") for id in cache.get(ids)]
            add_to_cache(f"tweets_for_user_{user_id}_chunk_{chunk}", tweets)
        else:
            return "No tweets for that user or no such chunk!"
    
    if tweets:
        return tweets 

def get_from_cache(first_key, second_key):
    return [cache.get(second_key + str(res)) for res in cache.get(first_key) if cache.get(first_key)]
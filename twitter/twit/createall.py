from ast import arg
from readline import insert_text
from django.db.models.expressions import F
from .models import Tweets, Users, LikedTweets, Followers, Retweets, Comments, LikedComments
from random_username.generate import generate_username
import random
import time
import datetime
from celery import shared_task

tags = ['#sport', '#music', "#travels", "#photo", '#it', "#russia"]

def get_random_date():
    year = datetime.datetime.now().year
    today = datetime.datetime.now().timetuple().tm_yday
    try:
        return datetime.datetime.strptime(
            f'{random.randint(1, today)} {year} \
            {random.randint(0, 23)}:{random.randint(0, 59)}:{random.randint(0, 59)}',
            '%j %Y %H:%M:%S'
            ).replace(tzinfo=datetime.timezone.utc)
    except ValueError:
        return get_random_date(year)

def get_list_of_users(start,stop):
    return Users.objects.filter(id__in=[i for i in range(start, stop)])

def list_of_users_with_random(users_number, total_users_number):
    return Users.objects.filter(id__in=[random.randint(0,total_users_number) for _ in range(users_number)])

@shared_task
def create_users(*args):
    start_time = time.perf_counter()
    Users.objects.bulk_create([
        Users(name=generate_username(1)[0],user_id=int(i),followers=random.randint(1, int(args[2])/4)) for i in range(args[0], args[1])
    ])
    return f'Users created! Time = {time.perf_counter() - start_time}'

@shared_task
def create_followers(*args):
    start_time = time.perf_counter()
    Followers.objects.bulk_create([
            Followers(user=follower, target_id=user) 
            for user in get_list_of_users(args[0],args[1])
            for follower in list_of_users_with_random(int(user.followers), args[2])
    ])

    return f'Followers created! Time = {time.perf_counter() - start_time}'

@shared_task
def create_random_tweet(*args):
    start_time = time.perf_counter()
    Tweets.objects.bulk_create([
        Tweets(
            user=user, 
            text=f'This is a sample tweet number {tweet_number} from user {user}', 
            likes_number=random.randint(0,args[2]/10), 
            views=random.randint(0,int(args[2])), 
            retweets_number=random.randint(0,args[2]/10),
            comments_number = random.randint(0,args[2]/10),
            published=get_random_date(),
            tags = random.choice(tags)
        ) 
        for user in get_list_of_users(args[0],args[1])
        for tweet_number in range(random.randint(1,args[2]/10))
    ])
    return f'Tweets created! Time = {time.perf_counter() - start_time}'

@shared_task
def create_tweets_likes(*args):
    start_time = time.perf_counter()
    LikedTweets.objects.bulk_create([
        LikedTweets(
            user_id=user, 
            tweet_id=tweet
        )
        for tweet in Tweets.objects.filter(user_id__in=get_list_of_users(args[0],args[1]))
        for user in list_of_users_with_random(int(tweet.likes_number),args[2])
    ])

    Retweets.objects.bulk_create([
        Retweets(
            user_id=user, 
            tweet_id=tweet, 
            retweet_date=get_random_date()
        )
        for tweet in Tweets.objects.filter(user_id__in=get_list_of_users(args[0],args[1]))
        for user in list_of_users_with_random(int(tweet.retweets_number),args[2])
    ])

    Comments.objects.bulk_create([
        Comments(
            user = user,
            text = f'This is a sample comment for tweet {tweet.id} from user {user}',
            likes_number = random.randint(0,args[2]/10),
            published = get_random_date(),
            tweet_id = tweet
        )
        for tweet in Tweets.objects.filter(user_id__in=get_list_of_users(args[0],args[1]))
        for user in list_of_users_with_random(int(tweet.comments_number),args[2])
    ])
    return f'Likes created! Time = {time.perf_counter() - start_time}'

@shared_task
def create_comments_likes(*args):
    start_time = time.perf_counter()
    array = []
    for _ in (
        LikedComments(
            user_id=user,
            comment_id=comment
        ) 
        for comment in Comments.objects.filter(user__in=get_list_of_users(args[0],args[1]))
        for user in list_of_users_with_random(int(comment.likes_number),args[2])
    ):
        if len(array) < 10000:
            array.append(_)
        else:
            LikedComments.objects.bulk_create(array)
            array = []
    return f'Likes for comments created! Time = {time.perf_counter() - start_time}'
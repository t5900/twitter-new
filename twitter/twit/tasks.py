from django.core.cache import cache
from .createall import *
from celery.result import AsyncResult
from .cache import *
import time
from .getters import get_all_users

def check_pending(dictionary):
    check = 'PENDING' in dictionary.values()
    while check == True:
        for id in dictionary:
            result = AsyncResult(id)
            dictionary[id] = result.status
        check = 'PENDING' in dictionary.values()

def slice_list(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

@shared_task
def create_task_master(number):
    cache.clear()
    total_time = time.perf_counter()
    coef = 50
    workers_number = int(number/coef)
    functions = {
        create_users: workers_number,
        create_followers: workers_number,
        create_random_tweet: workers_number,
        create_tweets_likes: workers_number,
        create_comments_likes: workers_number,
    }

    cache_functions = [
        cache_users,
        cache_tweets,
        cache_tags,
        cache_user_comments,
        cache_followers_lists,
        cache_subs_lists,
        cache_user_tweets_lists,
        cache_users_who_liked_tweet_lists,
        cache_comments_for_tweet
    ]

    cache_func_tasks = {}

    for func, workers_number in functions.items():
        temp_dict = {}
        start = 1
        end = coef

        for i in range(workers_number):
            task = func.delay(start, end, number)
            temp_dict[task.id] = task.status
            start = end
            end += coef

        check_pending(temp_dict)

    users = get_all_users()
    users_number = users.get('users_number')

    for func in cache_functions:
        
        task = func.delay(users)
        cache_func_tasks[task.id] = task.status

    for user in range(1,users_number):
        task = cache_tweets_letters.delay(user)
        cache_func_tasks[task.id] = task.status

    for lst in slice_list(get_all_comments().get('comments'), 999):
        task = cache_comments.delay(lst)
        cache_func_tasks[task.id] = task.status
   
    check_pending(cache_func_tasks)
    
    return f'Total time = {time.perf_counter() - total_time}'
from django.db import models
from django.contrib.auth.models import User

class Tweets(models.Model):

    user = models.ForeignKey('Users', on_delete=models.CASCADE, verbose_name= 'Пользователь', db_index=True)
    text = models.CharField(max_length=280, verbose_name= 'Текст')
    likes_number = models.IntegerField(verbose_name= 'Количество лайков')
    published = models.DateTimeField(verbose_name= 'Опубликовано')
    views = models.IntegerField(verbose_name= 'Количество просмотров', null=True)
    retweets_number = models.IntegerField(verbose_name= 'Количество ретвитов', null=True)
    comments_number = models.IntegerField(verbose_name= 'Количество комментариев', null=True)
    tags = models.CharField(max_length=280, verbose_name= 'Теги', null=True)

    class Meta :
        verbose_name_plural = 'Tweets' 
        verbose_name = 'Tweet' 
        ordering = ['-published']

    def __str__(self):
        return self.text

class Comments(models.Model):

    user = models.ForeignKey('Users', on_delete=models.CASCADE, verbose_name= 'Пользователь')
    text = models.CharField(max_length=280, verbose_name= 'Текст комментария')
    likes_number = models.IntegerField(verbose_name= 'Количество лайков')
    published = models.DateTimeField(verbose_name= 'Опубликовано')
    tweet_id = models.ForeignKey('Tweets', on_delete=models.CASCADE, verbose_name= 'Твит', db_index=True)

    class Meta :
        verbose_name_plural = 'Comments' 
        verbose_name = 'Comment' 
        ordering = ['-published']

    def __str__(self):
        return self.text

class Users(models.Model):
    name = models.CharField(max_length=255, null=True)
    user_id = models.IntegerField(verbose_name= 'ID Пользователя', null=True)
    followers = models.IntegerField(verbose_name= 'Количество подписчиков', null=True)

    def __str__(self):  
        return self.name

    class Meta :
        verbose_name_plural = 'Users' 
        verbose_name = 'User' 
        ordering = ['name']

class Retweets(models.Model):
    user_id = models.ForeignKey('Users', on_delete=models.CASCADE, verbose_name= 'Пользователь')
    tweet_id = models.ForeignKey('Tweets', on_delete=models.CASCADE, verbose_name= 'Твит', related_name='Tweets', db_index=True)
    retweet_date = models.DateTimeField(verbose_name= 'Опубликовано', null=True)

class LikedTweets(models.Model):
    user_id = models.ForeignKey('Users', on_delete=models.CASCADE, verbose_name= 'Пользователь')
    tweet_id = models.ForeignKey('Tweets', on_delete=models.CASCADE, verbose_name= 'Понравившийся твит', db_index=True)

class Followers(models.Model):
    user = models.ForeignKey('Users', on_delete=models.CASCADE, verbose_name= 'Пользователь', related_name='Friends', db_index=True)
    target_id = models.ForeignKey('Users', on_delete=models.CASCADE, verbose_name= 'Подписан на', related_name='Followers', db_index=True)

class LikedComments(models.Model):
    user_id = models.ForeignKey('Users', on_delete=models.CASCADE, verbose_name= 'Пользователь')
    comment_id = models.ForeignKey('Comments', on_delete=models.CASCADE, verbose_name= 'Понравившийся комментарий', db_index=True)
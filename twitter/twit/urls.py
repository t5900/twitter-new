from django.db import router
from django.urls import path,include

from twit import views

urlpatterns = [
    path('create/<int:users_number>', views.CreateContent.as_view()),
    path('task_status', views.JobStatus.as_view()),
    path('get_followers/<int:user_id>', views.ShowFollowers.as_view()),
    path('get_subscriptions/<int:user_id>', views.ShowSubscriptions.as_view()),
    path('tweetsletter/<int:user_id>/<int:chunk>', views.TweetLetter.as_view()),
    path('usertweets/<int:user_id>', views.UserTweets.as_view()),
    path('wholikedtweet/<int:tweet_id>', views.GetWhoLikedTweet.as_view()),
    path('tweetsbytag/<str:tag>', views.GetTweetsByTag.as_view()),
    path('usercomments/<int:user_id>', views.GetUserComments.as_view()),
    path('tweetcomments/<int:tweet_id>', views.GetCommentsForTweet.as_view()),
    path('commentlikes/<int:comment_id>', views.GetUsersWhoLikedComment.as_view()),
]
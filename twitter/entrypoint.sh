#!/bin/bash
bash migrations.sh
gunicorn --bind 0.0.0.0:8000 twitter.wsgi \
--name twitter \
--workers 3 \
--timeout 120 
import requests
import random
from multiprocessing import Pool
import os
import sys

server = str(os.environ.get("SERVER"))
users_number = int(os.environ.get("USERS_NUMBER"))
urls = [
    'get_followers/',
    'get_subscriptions/',
    'tweetsletter/',
    'usertweets/',
    'wholikedtweet/',
    'tweetsbytag/',
    'usercomments/',
    'tweetcomments/',
    'commentlikes/'
]

tags = ['#sport', '#music', "#travels", "#photo", '#it', "#russia"]

def get(server, user_id, url):
    answer = requests.post(
        f'{server}/{url}', json={
            'user_id': user_id,
            'tweet_id': user_id,
            'tag': random.choice(tags),
            'comment_id': user_id,
            'chunk': 1
            }
    )
    return answer.json()

def mainf(server, users_number):
    while True:
        user_id = random.choice(range(1,users_number))
        url = random.choice(urls)
        get(server,user_id,url)
        
print('Started')
with Pool() as pool:
    pool.starmap(mainf, [(server,users_number)])
    